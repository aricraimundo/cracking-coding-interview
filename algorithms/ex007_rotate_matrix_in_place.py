"""
1.7 Given an image represented by a NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the image by 90 degress.
    Can you do this in place?

    m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    rotate_matrix_in_place(m)
    print(m) -> [[7, 4, 1], [8, 5, 2], [9, 6, 3]]

"""


def rotate_matrix_in_place(m):

    n = len(m)

    if n == 0 or n != len(m[0]):
        return False

    for layer in range(0, int(n / 2)):
        first = layer
        last = n - 1 - layer

        for i in range(first, last):
            offset = i - first

            # save top
            top = m[first][i]

            # left -> top
            m[first][i] = m[last - offset][first]

            # bottom -> left
            m[last - offset][first] = m[last][last - offset]

            # right -> bottom
            m[last][last - offset] = m[i][last]

            # top -> right
            m[i][last] = top

    return True
