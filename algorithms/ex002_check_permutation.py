"""
1.2 Given two strings, write a method to decide if one is a permutation of the other.

is_permutation("abc", "bac") -> True
is_permutation("abc", "def") -> False

"""


def is_permutation(str1, str2):
    if len(str1) != len(str2):
        return False

    hash = {}
    for c in str1:
        if c in hash:
            hash[c] += 1
        else:
            hash[c] = 1

    for c in str2:
        if not c in hash:
            return False
        hash[c] -= 1
        if hash[c] < 0:
            return False

    return True
