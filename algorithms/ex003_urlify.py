"""
1.3 Replaces all spaces in a string with '%20'. You may assume that the string has sufficient space at the end 
    to hold the additional characters and that you are given the "true" length of a string.

urlify("Mr John Smith    ") -> "Mr%20John%20Smith"

"""

import ctypes


def urlify(s, true_length):
    m = ctypes.create_string_buffer(len(s))

    space_count = 0
    for i in range(true_length):
        if s[i] == " ":
            space_count += 1

    index = true_length + space_count * 2

    for i in reversed(range(true_length)):
        if s[i] == " ":
            m[index - 1] = b"0"
            m[index - 2] = b"2"
            m[index - 3] = b"%"
            index -= 3
        else:
            m[index - 1] = str.encode(s[i])
            index -= 1

    return m.value.decode()
