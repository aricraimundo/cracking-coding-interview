"""
1.5 There are three types of edits that can be performed on strings: insert a character, remove
    a character, or replace a character. Given two strings, write a function to check if they
    are one edit (or zero edits) away.

is_zero_or_one_edits_away("pale",  "ple") -> True
is_zero_or_one_edits_away("pales", "pale") -> True
is_zero_or_one_edits_away("pale",  "bale") -> True
is_zero_or_one_edits_away("pale",  "bake") -> False

"""


def is_zero_or_one_edits_away(str1, str2):
    len_diff = len(str2) - len(str1)
    if abs(len_diff) > 1:
        return False

    # zero edits
    if str1 == str2:
        return True

    # user has replaced a character
    if len_diff == 0:

        found_difference = False
        for index in range(len(str1)):

            if str1[index] != str2[index]:
                if found_difference:
                    return False
                found_difference = True

        return True

    else:

        user_has_deleted = (len_diff < 0)

        index1 = 0
        index2 = 0
        found_difference = False

        while (index1 < len(str1) and index2 < len(str2)):
            if str1[index1] != str2[index2]:
                if found_difference:
                    return False
                found_difference = True
            else:
                if user_has_deleted:
                    index2 += 1
                else:
                    index1 += 1

            if user_has_deleted:
                index1 += 1
            else:
                index2 += 1

        return True
