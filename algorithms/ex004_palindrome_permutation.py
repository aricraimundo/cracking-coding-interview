"""
1.4 Given a string, write a function to check if it is a permutation of a palindrome. A palindrome
    is a word or phrase that is the same forwards and backwards. A permutation is a rearrangement
    of letters. The palindrome does not need to be limited to just dictionary words.

is_palindrome_perm("Tact Coa") -> True ("taco cat" or "atco cta")

"""


def is_palindrome_perm(s):
    hash = {}
    count_odd = 0
    s = s.lower()
    for c in s:
        if c.isalpha():
            if c in hash:
                hash[c] += 1
            else:
                hash[c] = 1

            if hash[c] % 2 == 1:
                count_odd += 1
            else:
                count_odd -= 1

    return count_odd <= 1
