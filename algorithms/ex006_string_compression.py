"""
1.6 Implement a method to perform basic string compression using the counts of repeated characters. For example, the string aabcccccaaa
    would become a2b1c5a3. If the "compressed" string would not become smaller than the original string, your method should return the
    original string. You can assume the string has only uppercase and lowercase letters (a-z).
    
compress_string("aabcccccaaa") -> "a2b1c5a3"
compress_string("aabc") -> a2bc -> "aabc"

"""


def compress_string(s):

    if len(s) <= 1:
        return s

    output = []
    output.append(s[0])
    last_char = s[0]
    counter = 1

    for i in range(1, len(s)):

        if s[i] == last_char:
            counter += 1
        else:
            output.append(str(counter))
            output.append(s[i])
            last_char = s[i]
            counter = 1

    output.append(str(counter))
    output_str = ''.join(output)

    return output_str if len(output_str) < len(s) else s
