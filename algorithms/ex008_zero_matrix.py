"""
1.8 Write an algorithm such that if an element in MxN matrix is zero, its entire row and
    column are set to 0.

"""


def zero_matrix(m):

    if len(m) == 0 or len(m[0]) == 0:
        return

    num_rows = len(m)
    num_cols = len(m[0])

    rows_flag = [0] * num_rows
    cols_flag = [0] * num_cols

    for i in range(num_rows):
        if rows_flag[i] == 0:
            for j in range(num_cols):
                if m[i][j] == 0:
                    rows_flag[i] = 1
                    cols_flag[j] = 1

    for i in range(num_rows):
        for j in range(num_cols):
            if rows_flag[i] == 1 or cols_flag[j] == 1:
                m[i][j] = 0

    return
