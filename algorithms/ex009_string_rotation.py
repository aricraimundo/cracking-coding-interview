"""
1.9 Assume you have a method isSubstring which checks if one word is a substring of another. Given
    two strings, s1 and s2, write code to check if s2 is a rotation of s1, using only one call to
    isSubstring (Ex: waterbottle is a rotation of erbottlewat)

"""


def is_substring(s1, s2):
    """
    Checks if s1 is a substring of s2.
    """
    return s1 in s2


def is_rotation(s1, s2):
    """
    Checks if s2 is a rotation of s1
    """

    if len(s1) != len(s2):
        return False

    s = s1 + s1

    return is_substring(s2, s)
