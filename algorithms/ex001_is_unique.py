"""
1.1 Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

is_unique("abc") -> True
is_unique("a") -> True
is_unique("aba") -> False

"""


def is_unique(str_to_check):
    NUM_CHARACTERS = 256

    if str_to_check is None or len(str_to_check) == 0 or len(str_to_check) > NUM_CHARACTERS:
        return False
    elif len(str_to_check) == 1:
        return True
    else:
        char_arr = [False] * NUM_CHARACTERS
        for character in str_to_check:
            value = ord(character)
            if char_arr[value]:
                return False
            char_arr[value] = True

        return True


def is_unique_bit_vector(str_to_check):

    if str_to_check is None or len(str_to_check) == 0:
        return False
    elif len(str_to_check) == 1:
        return True

    bit_vector = 0
    for character in str_to_check:
        value = 1 << ord(character)
        if (bit_vector & value) > 0:
            return False
        bit_vector = bit_vector | value

    return True
