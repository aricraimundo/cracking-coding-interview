class SinglyLinkedList:

    def __init__(self):
        self.head = None

    def add(self, value):
        n = self.Node(value)

        if self.head is None:
            self.head = n
        else:
            node = self.head
            while node.next is not None:
                node = node.next
            node.next = n

    def delete(self, value):

        if self.head is None:
            return None
        elif self.head.value == value:
            node = self.head
            self.head = self.head.next
            return node
        else:
            node = self.head
            while node.next is not None:
                if node.next.value == value:
                    n = node.next
                    node.next = node.next.next
                    return n
        return None

    class Node:

        def __init__(self, value):
            self.value = value
            self.next = None
