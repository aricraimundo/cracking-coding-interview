# craking-coding-interview

This repository will have the questions and solutions of the book Cracking the Coding Interview from Gayle Laakmann Mcdowell.

https://www.amazon.com/dp/0984782850/ref=cm_sw_r_tw_dp_SD1ZP3AWMBJR10C972XX

Run tests using the following command:

```
python3 -m unittest
```
