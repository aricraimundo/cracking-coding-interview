import unittest

from data_structures.singly_linked_list import SinglyLinkedList


class SinglyLinkedListTest(unittest.TestCase):

    def test_empty_list_must_have_empty_node(self):

        # arrange
        linked_list = SinglyLinkedList()

        # assert
        self.assertIsNone(linked_list.head, 'Head should be None')

    def test_add_to_empty_list(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        linked_list.add(5)

        # assert
        self.assertIsNotNone(linked_list.head, 'Head should be valid')
        self.assertEqual(linked_list.head.value, 5, 'Head should be 5')

    def test_add_values(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        linked_list.add(5)
        linked_list.add(10)
        linked_list.add(15)

        # assert
        self.assertEqual(linked_list.head.value, 5, 'Head should be 5')
        self.assertEqual(linked_list.head.next.value,
                         10, 'Second node should be 10')
        self.assertEqual(linked_list.head.next.next.value,
                         15, 'Third node should be 15')

    def test_delete_empty_list(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        n = linked_list.delete(5)

        # assert
        self.assertIsNone(n, 'Return must be None')

    def test_delete_head(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        linked_list.add(5)
        n = linked_list.delete(5)

        # assert
        self.assertIsNotNone(n, 'Return must be valid')
        self.assertEqual(n.value, 5, 'Return must be 5')
        self.assertIsNone(linked_list.head, 'Head must be None')

    def test_delete_last(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        linked_list.add(5)
        linked_list.add(15)
        n = linked_list.delete(15)

        # assert
        self.assertIsNotNone(n, 'Return must be valid')
        self.assertEqual(n.value, 15, 'Return must be 15')

    def test_delete_middle(self):

        # arrange
        linked_list = SinglyLinkedList()

        # act
        linked_list.add(5)
        linked_list.add(10)
        linked_list.add(15)
        n = linked_list.delete(10)

        # assert
        self.assertIsNotNone(n, 'Return must be valid')
        self.assertEqual(n.value, 10, 'Return must be 10')
        self.assertIsNotNone(linked_list.head, 'Head must be valid')
        self.assertEqual(linked_list.head.value, 5, 'Head must be 5')
