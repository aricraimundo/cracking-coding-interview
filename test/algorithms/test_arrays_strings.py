import unittest

from algorithms.ex001_is_unique import *
from algorithms.ex002_check_permutation import *
from algorithms.ex003_urlify import *
from algorithms.ex004_palindrome_permutation import *
from algorithms.ex005_one_away import *
from algorithms.ex006_string_compression import *
from algorithms.ex007_rotate_matrix_in_place import *
from algorithms.ex008_zero_matrix import *
from algorithms.ex009_string_rotation import *


class ArraysAndStringsTest(unittest.TestCase):

    def test_is_unique(self):

        self.assertTrue(is_unique("abc"))
        self.assertTrue(is_unique("a"))
        self.assertFalse(is_unique("aba"))

    def test_check_permutation(self):

        self.assertTrue(is_permutation("abc", "bac"))
        self.assertFalse(is_permutation("abc", "def"))

    def test_urlify(self):

        self.assertEqual(urlify("Mr John Smith    ", 13), "Mr%20John%20Smith")
        self.assertEqual(urlify("Michael Jordan  ", 14), "Michael%20Jordan")

    def test_palindrome_permutation(self):

        self.assertTrue(is_palindrome_perm("Tact Coa"))
        self.assertFalse(is_palindrome_perm("code"))
        self.assertTrue(is_palindrome_perm("aab"))
        self.assertTrue(is_palindrome_perm("Carerac"))

    def test_one_away(self):

        self.assertTrue(is_zero_or_one_edits_away("pale",  "ple"))
        self.assertTrue(is_zero_or_one_edits_away("pales", "pale"))
        self.assertTrue(is_zero_or_one_edits_away("pale",  "bale"))
        self.assertFalse(is_zero_or_one_edits_away("pale",  "bake"))

    def test_compress_string(self):

        self.assertEqual(compress_string("aabcccccaaa"), "a2b1c5a3")
        self.assertEqual(compress_string("aabc"), "aabc")
        self.assertEqual(compress_string("bcceeeeeeh"), "b1c2e6h1")

    def test_rotate_matrix_in_place(self):

        m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        r = rotate_matrix_in_place(m)
        self.assertTrue(r)
        self.assertEqual(m, [[7, 4, 1], [8, 5, 2], [9, 6, 3]])

    def test_zero_matrix(self):

        m = [[0, 2], [3, 4]]
        zero_matrix(m)
        self.assertEqual(m, [[0, 0], [0, 4]])

        m = [[0, 2, 1], [3, 4, 8], [5, 6, 7]]
        zero_matrix(m)
        self.assertEqual(m, [[0, 0, 0], [0, 4, 8], [0, 6, 7]])

        m = [[5, 2, 1], [3, 0, 8], [5, 6, 7]]
        zero_matrix(m)
        self.assertEqual(m, [[5, 0, 1], [0, 0, 0], [5, 0, 7]])

        m = [[5, 2, 1], [3, 3, 8], [5, 6, 0]]
        zero_matrix(m)
        self.assertEqual(m, [[5, 2, 0], [3, 3, 0], [0, 0, 0]])

    def test_is_rotation(self):

        self.assertTrue(is_rotation("waterbottle", "erbottlewat"))
        self.assertTrue(is_rotation("testing", "ngtesti"))
        self.assertTrue(is_rotation("xoxo", "xoxo"))
        self.assertTrue(is_rotation("python", "thonpy"))
        self.assertFalse(is_rotation("python", "pyonth"))
        self.assertFalse(is_rotation("test", "tes"))
